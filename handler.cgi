#!/usr/bin/bash

sqlfile="tx.db"

if [[ ! -e "${sqlfile}" ]]; then
  sqlite3 "${sqlfile}" 'CREATE TABLE "txheader" (id integer primary key, date text, items int, amount num, tender text, void text); CREATE TABLE txdetail (id integer, seller text, amount num, foreign key(id) references txhead(id));'
fi

oifs=$IFS
IFS=,; read -ra elements -n "${CONTENT_LENGTH}"
IFS=$oifs
#
# TODO: Need to avoid SQL injection attacks, here. Bail if anything looks fishy.
#
#
# Insert header record
#
cmd="sqlite3 ${sqlfile} 'insert into txheader (date, id, void, amount, tender, items) values(\"${elements[0]}\",${elements[1]},\"${elements[2]}\",${elements[3]},\"${elements[5]}\",${elements[6]});'"
echo $cmd | bash
#
# Insert detail records
#
detpos=7 # the first of the detail fields
detcount=0
cmd="sqlite3 ${sqlfile} 'insert into txdetail (id, amount, seller) values"
while [[ -n "${elements[$detpos]}" ]]; do
  cmd="${cmd}(${elements[1]},${elements[$detpos]},\"${elements[$((detpos+1))]}\")"
  detpos=$((detpos + 2))
  if [[ -n "${elements[$detpos]}" ]]; then
    cmd="${cmd},"
  fi
  detcount=$((detcount + 1))
done
if (($detcount > 0)); then
  cmd="${cmd}';"
  echo $cmd | bash
fi
# Send the HTTP response headers
echo -e  "Content-Type: text/plain\n"
