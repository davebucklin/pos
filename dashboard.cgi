#!/usr/bin/bash

# Send the HTTP response headers
echo -e  "Content-Type: text/plain\n"

# Sales by date/seller with item count
{ echo -e "Date | Seller | Items | Sales\n--- | --- | --: | --:"
sqlite3 tx.db "select date(h.date), d.seller, count(*), format('$%.2f',total(d.amount)) from txdetail d, txheader h where d.id = h.id and h.void='N' group by date(h.date), d.seller;" ; } | pandoc -fmarkdown -tplain
echo

# Sales by seller
{ echo -e "Seller | Sales\n--- | --:"
sqlite3 tx.db "select d.seller, format('$%.2f',total(d.amount)) from txdetail d, txheader h where d.id = h.id and h.void='N' group by d.seller;" ; } | pandoc -fmarkdown -tplain
echo

# Total Sales
{ echo -en "TOTAL Sales: "; sqlite3 tx.db "select format('$%.2f',total(amount)) from txheader where void='N';" ; }
