#!/usr/bin/bash
# When the transaction file changes, send new transactions to remote system.
#
# Add this to your crontab:
# */5 * * * * cd pos; bash monitor.sh

# change to user@host
remote=""

filename="tx.csv"
[ -f "$filename" ] || { echo "$filename not found. Exiting." >&2; exit 1; }

lastfile=$filename".last"
ts=$(date +%Y%m%d%H%M%S)

[ -f "$lastfile" ] || touch "$lastfile"

cmp --silent "$filename" "$lastfile" || {
  cp "$filename" "$lastfile"
  files=("$filename" "$filename$ts")
  cp "${files[@]}"
  [ -n "$remote" ] && scp "${files[@]}" "${remote}:" ; }
