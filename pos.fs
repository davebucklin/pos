\ pos.fs
\ capture sales transactions, output to file

\ input buffer and offset pointer
create input 2 cells allot
variable in-offset

\ key-function map
char a constant k_void
char b constant k_bs
char c constant k_seller1
char d constant k_seller2
char e constant k_seller3
char f constant k_seller4
char g constant k_seller5
char h constant k_venmo
char i constant k_cash

\ sellers and tenders - should probably come from a config file
variable #sellers 5 #sellers !
create sellers k_seller1 , k_seller2 , k_seller3 , k_seller4 , k_seller5 ,
create $names #sellers @ cells allot
s" Maranda Dave    Sarah   Seller4 Seller5 " $names swap cmove

create tenders k_cash , k_venmo , 0 ,
create $tenders 3 cells allot
s" Cash    Venmo   VOID    " $tenders swap cmove

\ transaction data
variable seller
variable tender
variable total
variable void   \ bool
variable txid
variable #items \ number of items
variable >item  \ pointer to manifest head

\ manifest in heap memory
128 cells allocate throw constant manifest
variable mp \ manifest data pointer

\ output buffer
512 allocate throw constant outbuf

\ screen layout constants
17 constant bottom-line
bottom-line 1 + constant price-line
bottom-line 3 + constant total-line
bottom-line 5 + constant tender-line
bottom-line 6 + constant change-line
bottom-line 7 + constant msg-line
14 constant pagelen

: ++ ( addr -- )
  1 swap +! ;

: -- ( addr -- )
  dup @ 1 - swap ! ;

\ reset input buffer
: input-reset ( -- )
  0 in-offset !
  input 2 cells erase ;

\ reset transaction values
: new ( -- )
  \ manifest 128 cells erase
  manifest mp !
  0 total !
  0 void !
  0 tender !
  0 seller !
  0 #items !
  0 >item !
  txid ++
  outbuf 512 erase ;

\ convert buffer to number
: input>number ( -- n )
  in-offset @ 0= if 0 else input in-offset @ s>number? invert throw drop then ;

\ is this char 0-9 on the keyboard?
: numeric? ( char -- f )
  48 58 within ;

\ get the starting tx ID
: get-tx ( -- )
  page
  ." Starting transaction number: "
  4 0 do
    key dup emit input i + c!
  loop
  4 in-offset !
  input>number 1- txid ! ;

\ reset variables to initial state
: init ( -- )
  s" touch tx.csv" system get-tx input-reset ;

\ is the incoming value a seller value?
: seller? ( n -- f )
  0 #sellers @ 0 do over sellers i cells + @ = + loop nip ;

\ is the incoming value a tender type?
: tender? ( n -- f )
  0 2 0 do over tenders i cells + @ = + loop nip ;

: control? ( char -- f )
  dup dup seller? swap tender? or swap k_void = or ;

\ store numeric char in buffer, advance pointer
: >input ( char -- )
  dup numeric? if
    input in-offset @ + c!
    in-offset ++
  else drop then ;

\ monetary output, taken from gforth manual
: dollars ( n -- addr len )
  0              \ convert to unsigned double
  <<#            \ start conversion
  # #            \ convert two least-significant digits
  '.' hold       \ insert decimal point
  #s             \ convert remaining digits
\  '$' hold       \ append currency symbol
  #>             \ complete conversion
  #>> ;

\ money output
: .number ( n -- )
  dollars type space ;

\ total line
: .total ( -- )
  total @ 3 total-line at-xy ." TOTAL: " .number ;

\ handle backspace to the input buffer
: bs>input ( -- )
  in-offset @ 0= invert if
    in-offset --
    0 input in-offset @ + c!
  then ;

\ price line
: .price ( -- )
  10 price-line at-xy input>number .number ;

\ stuff total amount into input field
: tender-total ( -- )
  total @ 0 <# #s #> dup in-offset !
  input swap cmove ;

\ store value at mp position, advance mp
: mp! ( n -- )
  mp @ ! cell mp +! ;

\ add current line item to list of items on transaction
: add ( -- )
  mp @
  >item @ mp! input>number mp! seller @ mp!
  >item !
  #items ++ ;

: ring-item ( seller -- )
  seller ! add input>number total +! .total ;

: set-tender ( tender -- )
  tender ! in-offset @ 0= if tender-total then ;

\ manifest stores the list of sale items

\ fetch item data
: data> ( addr -- n char )
  dup cell + @ swap 2 cells + @ ;

\ fetch forward link
: next> ( addr -- addr' )
  @ ;

\ get seller name by id
: seller>name ( seller id -- addr len )
  #sellers @ 0 do
    dup sellers i cells + @ = if drop i 8 * $names + 8 leave then
  loop ;

: rtrim ( addr len -- addr len )
  begin 2dup + 1- c@ bl = while 1- repeat ;

: .name ( seller-id -- )
  seller>name type ;

\ print items on the sale until all or printed or pagelen is exceeded
: .manifest ( -- )
  >item @ 1 ( addr idx )
  begin
    dup bottom-line swap - 0 swap at-xy
    swap dup data> .name space space .number cr next>
    swap 1+
  over 0 = over pagelen > or until
  drop 0 <> if 8 bottom-line pagelen 1+ - at-xy ." ^" then ;
  
\ translate tender code into string value
: tendertx ( tender -- addr len )
  3 0 do
    dup tenders i cells + @ = if drop i leave then
  loop
  8 * $tenders + 8 rtrim ;

: .tender ( -- )
  input>number .number
  tender @ tendertx type ;

: .change ( -- )
  2 change-line at-xy ." Change: " input>number total @ - .number ;

\ time stamp builder
: year ( u -- )     0 <# # # # # #> outbuf swap cmove ;
: month ( u -- )    0 <# # # #> [char] - outbuf 4 + c! outbuf 5 + swap cmove ;
: day ( u -- )      0 <# # # #> [char] - outbuf 7 + c! outbuf 8 + swap cmove ;
: hour ( u -- )     0 <# # # #> bl outbuf 10 + c! outbuf 11 + swap cmove ;
: minute ( u -- )   0 <# # # #> [char] : outbuf 13 + c! outbuf 14 + swap cmove ;
: timestamp ( -- )  time&date year month day hour minute drop ;

: $txid ( -- addr len )
  txid @ 0 <<# # # # # #> #>> ;

: .timestamp ( -- )
  0 0 at-xy outbuf 16 type ;

: clear ( -- )
  page timestamp .timestamp .total 20 0 at-xy $txid type ;

\ export transaction to a file

variable >tx \ tx buffer offset
: comma ( -- )
  [char] , outbuf >tx @ + c! >tx ++ ;

\ tack string onto the end of the outbuf, increment >tx
: tack ( addr len -- )
  tuck outbuf >tx @ + swap cmove >tx @ + >tx ! comma ;

: n>string ( n -- addr len )
  0 <# #s #> ;

\ build transaction output buffer
\ format: datetime[1-16], txid, void?[18], total, tendered amt, tender type, # of items, item-amt, item-seller, ...,
: stage ( -- )
  16 >tx ! comma
  $txid tack
  void @ if s" Y" else s" N" then tack
  total @ dollars tack
  input>number dollars tack \ tendered amt
  tender @ tendertx tack
  #items @ n>string tack
  \ add items
  >item @ begin
  dup while
    dup data> swap dollars tack seller>name rtrim tack next>
  repeat drop ;

: open ( addr u -- file-id ) r/w open-file throw ;

\ skip to the end
: ffwd ( fileid -- )
  dup file-size throw rot reposition-file throw ;

: write ( -- )
  s" tx.csv" open dup ffwd dup outbuf >tx @ rot write-line throw close-file throw ;

: save ( -- )
  stage write ;

\ get a line item, seller
\ get tendered amount and type
: get ( -- char )
  input-reset
  begin
    key
  dup control? invert while
    dup k_bs = if drop bs>input else >input then
    .price
  repeat ;

: pos ( -- )
  init
  begin
    new clear
    begin
      get
    dup seller? while
      ring-item .manifest input-reset .price
    repeat
    timestamp .timestamp
    dup tender? if
      dup set-tender
      0 tender-line at-xy ." Tendered: " .tender .change
    then
    0 msg-line at-xy
    k_void = if
      -1 void !
      0 msg-line at-xy
      $txid type ."  VOID. "
    then
    save
    1000 ms
    ." Press any key. " ( depth . ) key drop
  again ;

pos

