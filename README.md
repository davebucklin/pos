# What is this?

A POS terminal and transaction recording system for a multi-party garage sale.

Imagine, if you will, a mutli-party garage sale.
Each party sells items that have a colored sticker with the price on it.
Traditionally, stickers would be removed from each item and placed on a poster or in a book for each sale.
At the end of the day, total monies recieved can be tallied and distributed to each party.

Problems with this include:

* Having to handle the stickers
* Possibly losing stickers
* Having to tally up stickers at the end of the day

# Solution

Point of sale terminal that:

* Associates each line item amount with a selling party
  * Most importantly, keeps a running tally of monies per party
* Calculates total sale and change due
* Captures tendered amount and type 
  * Cash or venmo
* Records transactions in a transaction log
* Can sync files to remote system for backup

## How does it work?

User interface is a terminal screen and a cusomized keyboard.
Keyboard has a 10-key, plus buttons associated to each party.
(Probaby affix a sticker from each seller to a column of keys.)

### Operator can

* Enter a starting transaction ID number
* Begin a new transaction
* enter a line item
  * Enter amount
  * press seller's key to enter
  * Backspace to correct price
* POS displays running total
* Input amount tendered and type
  * Input amount
  * press tender type (cash, Venmo)
  * Pressing tender button without entering an amount assumes exact change was tendered
  * App displays change due (tendered amount - sale amount)
* Void a transaction and start over

Operator notes:

* No decimal. $2.25 is entered as 225. $.50 is entered as 50.
* If you key in the wrong price on an item,
  * You can void the sale and start over
  * If the price entered was lower than intended, add another item to make up the difference
  * If the price entered was higher than intended, subtract the difference from the next item from the same seller

## Technical implementation

### Required Hardware

* A system with display to act as the POS terminal
  * E.g., a 10" Android tablet running Termux
  * persistent internet connection not required
* Macro-pad 10-key with additional keys for sellers, tenders, void, backspace

# Installation

## Point-of-sale System

This is the system being used to capture transactions.
For example, this could be a tablet running Android and Termux.

Ensure gforth is installed.

`apt install gforth`

Then, clone this repository

`git clone https://gitlab.com/davebucklin/pos`

Now you can launch the POS interface

`cd pos; gforth pos.fs`

Transactions are stored in tx.csv.

## Remote System

If you only want to record transactions locally,
none of the following is necessary. 
In this section, you will set up the server-side application
so that transactions can be backed up
and sales data shared out.

<<<<<<< HEAD
### Server-side CGI

The handler.cgi CGI script accepts transactions from the client and adds them to an SQLite database.
The daschboard.cgi CGI script queries the database and outputs summary sales information.
I assume you already have apache installed and running and either a domain or IP address configured to point to the web server.

Install sqlite3 on the remote system.

`#apt install sqlite3`

Prepare a directory for CGI (e.g., /usr/lib/cgi-bin).
(Apache runs as www-data on my system.)
I used `setfacl -m g:www-data:rwx` on /usr /usr/lib and /usr/lib/cgi-bin so that www-data can read and write files and run scripts.

Clone this repo and copy the handler.cgi and dashboard.cgi files to /usr/lib/cgi-bin and make them executable by www-data.

`#chown www-data:www-data handler.cgi dashboard.cgi`

`#chmod u+x handler.cgi dashboard.cgi`

Configure your web server for CGI. I'm using Apache on Debian.
Enable CGI with `a2enmod cgi`.
Add the following to /etc/apache2/sites-available/000-default.conf

```
ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
<Directory "/usr/lib/cgi-bin/">
	AllowOverride None
	Options +ExecCGI
	AddHandler cgi-script .cgi .pl .py
	Require all granted
</Directory>
```

Restart apache with `systemctl restart apache2`

Test with `curl -d '2024-07-07,1000,N,10.00,10.00,Cash,2,5.00,Seller1,5.00,Seller2' https:/your-domain/cgi-bin/handler.cgi`

Then verify that the transaction data made its way into the database.

`sudo -u www-data sqlite3 /usr/lib/cgi-bin/tx.db "select * from txheader; select * from txdetail;"`

Back on the client machine, configure monitor.sh by changing the `remote=""` line to set it to the CGI script URL.
=======
On the client machine, configure monitor.sh by changing the `remote=""` line to set it to your user@host string.
>>>>>>> 9949db8 (revert config instructions)
It should look like this:

`remote="yoda@dagobah"`

Now, add the following line to the crontab on the POS terminal (using `crontab -e`):

`*/5 * * * * cd pos; bash monitor.sh`

The `monitor.sh` script will send any transactions that have not been sent already to the server.

You can view your sale dashboard at https://your-domain/cgi-bin/dashboard.cgi

# To-Do

After using this for a three-day yard sale with three sellers, 
I'm pretty happy with the front end.

## Front End

* Need a more elegant way to enter adjustments
  * Wrong item amount captured - adjust from money in back to seller
  * Wrong seller captured - adjust from one seller to another
* Store configuration data (e.g., seller names) in a config file, not in the code
* At the beginning of transaction entry, better indicate that the application is expecting an item amount
* Client should be usable on its own
  * Add some basic reporting scripts

## Other Ideas

* Maybe start creating docs using GitLab Pages
* I could have use cases, process diagrams, setup tutorial, etc
